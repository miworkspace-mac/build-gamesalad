#!/bin/bash -ex

# CONFIG
prefix="GameSalad"
suffix=""
munki_package_name="GameSalad"
display_name="GameSalad"
category="Developer Tools"
description="GameSalad is the world’s fastest game design engine. Learn to make and publish hit games for free on iPhone, iPad, Android & HTML5. No coding required."
url=`./finder.rb`

# download it (-L: follow redirects)
curl -L -o app.dmg -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

#convert DMG to CDR to bypass EULA
/usr/bin/hdiutil convert -quiet app.dmg -format UDTO -o app.cdr
rm -rf app.dmg

## Mount disk image on temp space
mountpoint=`hdiutil attach -mountrandom /tmp -nobrowse app.cdr | awk '/private\/tmp/ { print $3 } '`

## Unpack
# /usr/sbin/pkgutil --expand "${mountpoint}/*.pkg" pkg
mkdir -p build-root/Applications
app_in_dmg=$(ls -d $mountpoint/*.app)
cp -R $app_in_dmg build-root/Applications

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleVersion" $app_in_dmg/Contents/Info.plist`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`
# (cd build-root; pax -rz -f ../pkg/*/Payload)
hdiutil detach "${mountpoint}"

## Create pkg's
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version --scripts scripts app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -or -name '*.plugin' -or -name '*.prefPane' -or -name '*component' -maxdepth 3 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo -m go-w -g admin -o root app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.10.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" category -string "${category}"

# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"


# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
