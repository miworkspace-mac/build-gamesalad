#!/usr/bin/env ruby

require 'rubygems'
require 'nokogiri'
require 'open-uri'
require 'openssl'

begin
  doc = Nokogiri::HTML(open("https://gamesalad.com/download/studioUpdates", "User-Agent" => "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/46.0.2490.80 Safari/537.36", ssl_verify_mode: OpenSSL::SSL::VERIFY_NONE))
rescue Exception => e
  puts "Failed to fetch download page"
  puts e
  exit 1
end

link = doc.xpath('//guid').find {|x| x.inner_text.match(/.dmg/i)}
awks = link.inner_html.strip

if link
    puts "#{awks}"
end
